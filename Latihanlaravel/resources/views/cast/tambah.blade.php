@extends('layouts.master')

@section('judul')
    Halaman Tambah Cast
@endsection
@section('content')

<form action="/cast" method="POST">
    @csrf
    {{-- validation --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="form-group">
      <label for="exampleInputEmail1">Cast Name</label>
      <input type="text" name="name" class="form-control">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Cast Age</label>
        <input type="text" name="age" class="form-control">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Cast Bio</label>
        <input type="text" name="bio" class="form-control">
    </div>

    </div>
    <button type="submit" class="btn btn-primary ">Submit</button>
  </form>

@endsection