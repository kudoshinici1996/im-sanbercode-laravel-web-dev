@extends('layouts.master')

@section('judul')
    Halaman Tampil Cast
@endsection
@section('content')

<a class="btn btn-primary btn-sm my-3" href="/cast/create">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">name</th>
        <th scope="col">Age</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    @forelse ($cast as $index=> $casting)
    <tbody>
      <tr>
            <th scope="row">{{$index + 1}}</th>
            <td>{{$casting->name}}</td>
            <td>{{$casting->age}}</td>
            <td>
                <a href="/cast/{{$casting->id}}" class="btn btn-info btn-sm">detail</a>
                <a href="/cast/{{$casting->id}}/edit" class="btn btn-warning btn-sm">edit</a>
            </td>
        </tr>
        @empty
        <tr>
            <td>tidak ada casting</td>
        </tr>
    </tbody>
    @endforelse
</table>

@endsection

