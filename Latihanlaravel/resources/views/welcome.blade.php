@extends('layouts.master')

@section('judul')
    Halaman Utama
@endsection

@section('content')
    <h1>SanberBook</h1>
    <h2>Sosial Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagai ahar hidup ini semakin santai berkualitas</p>
    <h3>Benefit Join di SanberBook</h3>
    <ul>
        <li><p>Mendapatkan motivasi dari sesama developer</p></li>
        <li><p>Sharing knowledge dari para mastah Sanber</p></li>
        <li><p>Dibuat oleh calon web developer terbaik</p></li>
    </ul>
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li><p>Menjunjung website ini</p></li>
        <li><p>Mendaftar di <a href="/register">Form Sign up</a></p></li>
        <li><p>Selesai</p></li>
    </ol>
@endsection
