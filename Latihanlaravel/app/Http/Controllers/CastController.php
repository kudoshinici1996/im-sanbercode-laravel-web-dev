<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambah');
    }

    public function store(request $request){
        // validasi
        $validated = $request->validate([
            'name' => 'required|min:4',
            'age' => 'required',
            'bio' => 'required',
        ]);

        //tambah
        DB::table('cast')->insert([
            'name' => $request->input('name'),
            'age' => $request->input('age'),
            'bio' => $request->input('bio')
        ]);

        return redirect('/cast');
     
    }

    public function index(){
        $cast = DB::table('cast')->get();
        //dd($cast);

        return view('cast.tampil',['cast' => $cast]);
    }

    public function show($id){
        //dd($id);
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }
}
