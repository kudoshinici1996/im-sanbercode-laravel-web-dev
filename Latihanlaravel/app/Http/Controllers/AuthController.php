<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata()
    {
        return view('page.register');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request->input('firstname');
        $namaBelakang = $request->input('Lasttname');

        return view('page.end',["namaDepan" => $namaDepan,"namaBelakang" => $namaBelakang]);
    }
}
