<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'biodata']);

route::post('/welcome', [AuthController::class, 'welcome']);

route::get('/data-table', function(){
    return view('page.data-table');
});

route::get('/table', function(){
    return view('page.table');
});

//create data
//menuju form cast data
Route::get('/cast/create', [CastController::class, 'create']);
//masukan data ke database
route::post('/cast', [CastController::class, 'store']);

//read data
route::get('/cast', [CastController::class, 'index']);
//detail data berdasarlan param
route::get('/cast/{id}', [CastController::class, 'show']);
//edit data
route::get('/cast/{id}/edit', [CastController::class, 'edit']);