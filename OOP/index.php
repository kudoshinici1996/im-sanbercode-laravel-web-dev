<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");

echo "Name : " . $sheep ->name . "<br>";
echo "Legs : " . $sheep ->legs . "<br>";
echo "cold blooded : " . $sheep ->cold_blooded . "<br> <br> ";

$frog = new frog("buduk");

echo "Name : " . $frog ->name . "<br>";
echo "Legs : " . $frog ->legs . "<br>";
echo "cold blooded : " . $frog ->cold_blooded . "<br>";
echo $frog->jump();
echo "<br> <br>";

$ape = new ape("kera sakti");

echo "Name : " . $ape ->name . "<br>";
echo "Legs : " . $ape ->legs . "<br>";
echo "cold blooded : " . $ape ->cold_blooded . "<br>";
echo $ape->yell();

